package com.vehicle;

public interface Drivable {         //мы создаём интерфейс для транспортных средств
    void turn();                    //и описываем что эти транспортные средцтва делают
    void brake();                   // создаём методы поворот тормоз ускорение
    void accelerate();
}
