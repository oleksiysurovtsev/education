package com.vehicle;

public class GasTank {
    private Integer currentvalue;   //количество бензина на данный момент
    private Integer capacity;       // вместимость бензобака

    public Integer getCurrentvalue() {
        return currentvalue;
    }

    public void setCurrentvalue(Integer currentvalue) {
        this.currentvalue = currentvalue;
    }



    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }
}
