package com.vehicle;

import java.util.Scanner;

public class Car implements Drivable {              //имплементируем классу кар интерфейс драйвбл

//мы создали классы двигатель, колеса, бензобак.
// и теперь запихиваем их в машину.
    //модификатор доступа   тип переменной (класс)      имя переменной инициалезируем
            private         GasTank                       gasTank   = new GasTank();
            private         Engine                        engine    = new Engine();
            private         Wheels                        wheels    = new Wheels();

//создали конструктор в ктором дёрнули спецификацию
    public Car (){
        setSpecification();
    }


    @Override
    public void turn() {

    }

    @Override
    public void brake() {

    }

    @Override
    public void accelerate() {

    }











    //забиваем спецификацию (максимальная скорость, вместимость бака итд итп)


    private void setSpecification(){


        //сначала спецификацыю делаем для двигателя
        System.out.println("Введите максимальную скорость");
        Scanner sc = new Scanner(System.in);
        int maxpeed = sc.nextInt();

        engine.setMaxspeed(maxpeed); //взяли двигатель и передали в него максимальну скорость (присвоили сетером)




        System.out.println("Введите расход топлива");
        double consumption = sc.nextDouble();

        engine.setConsumption(consumption); //взяли двигло передали в него расход (присвоили сетером)


        //потом делаем для бензобака

        System.out.println("Введите ёмкость бензобака");
        Integer capacity = sc.nextInt();

        gasTank.setCapacity(capacity);


    }
}