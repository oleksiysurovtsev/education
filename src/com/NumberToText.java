package com;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/*Задание!
Написать программу которая будет спрашивать цифру и переводить её в прописной формат
*/
public class NumberToText {
    public static void main(String[] args) {
        //method1();
        //method2();
        method3();
    }

    public static void method1() {
        System.out.println("Добрый день! я переведу для вас цифру в прописной формат");
        System.out.println("Введите цифру от 0 до 9 ");
        String[] spelledout = {"ноль", "единицу", "двойку", "тройку", "чертвёрку", "пятёрку", "шестёрку", "семёрку", "восьмёрку", "девятку"};
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        for (int n = 0; n < spelledout.length; n++) {
            if (i == n) {
                System.out.println("Вы ввели " + spelledout[n]);
                break;
            }
        }
    }

    public static void method2() {
        HashMap mapnumb = new HashMap<Integer, String>(); //создаём колекцию Map

        mapnumb.put(0, "ноль");                           //заполлняем Map (ключ,значение)
        mapnumb.put(1, "еденица");
        mapnumb.put(2, "двойка");
        mapnumb.put(3, "тройка");
        mapnumb.put(4, "четвёрка");
        mapnumb.put(5, "пятёрка");
        mapnumb.put(6, "шестёрка");
        mapnumb.put(7, "семёрка");
        mapnumb.put(8, "восьмёрка");
        mapnumb.put(9, "девятка");

        System.out.println("Будте добры введите число");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println("вы ввели " + mapnumb.get(n));
    }

    public static void method3() {
        System.out.println("Добрый день!");
        System.out.println("Введите число");

        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        int n = 0;

        ArrayList<String> ged = new ArrayList<>();
        ged.add("ноль");
        ged.add("еденица");
        ged.add("двойка");
        ged.add("тройка");
        ged.add("четвёрка");
        ged.add("пятёрка");
        ged.add("шестёрка");
        ged.add("семёрка");
        ged.add("восьмёрка");
        ged.add("девятка");

        while (n < ged.size()) {
            if (n == i) {
                System.out.println(ged.get(i));
                break;
            }
            n++;
        }
    }
}

